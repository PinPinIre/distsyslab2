# Lab2Server.py
# Lab 2 CS4032
# Cathal Geoghegan #11347076

import socket
import threading
import Queue
import os
import httplib
import re
import sys

class TCPServer:
  PORT = 8000
  HOST = '0.0.0.0'
  LENGTH = 4096
  MAX_THREAD = 2

  def __init__(self, port_use=None):
    if not port_use:
      port_use = self.PORT
    self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    self.sock.bind((self.HOST, port_use))

    # Create a queue of tasks with ma
    self.threadQueue = Queue.Queue(maxsize=self.MAX_THREAD)

    #Create thread pool
    for i in range(self.MAX_THREAD):
      thread = ThreadHandler(self.threadQueue, self.LENGTH)
      thread.setDaemon(True)
      thread.start()
    self.listen()


  def listen(self):
    self.sock.listen(5)

    # Listen for connections and delegate to threads
    while True:
      con, addr = self.sock.accept()

      # If queue full close connection, otherwise send to thread
      if not self.threadQueue.full():
        self.threadQueue.put((con, addr))
      else:
        print "Queue full closing connection from %s:%s" % (addr[0], addr[1])
        con.close()



class ThreadHandler(threading.Thread):
  def __init__(self, threadQueue, bufferLength):
    threading.Thread.__init__(self)
    self.queue = threadQueue
    self.bufferLength = bufferLength

  def run(self):
    # Thread loops and waits for connections to be added to the queue
    while True:
      request = self.queue.get()
      self.handler(request)
      self.queue.task_done()

  def handler(self, (con, addr)):
    printData = ""
    # Loop and receive data
    while True:
      data = con.recv(1024)
      printData += data
      if (len(data) < self.bufferLength):
        break

    # Extract message body
    #message = re.split("\n\n", printData)
    message = printData

    # If valid http request with message body
    if len(message) > 0:
      if message == "KILL_SERVICE":
        print "Killing service"
        self.kill_serv(con)
      elif message[:4] == "HELO":
        self.helo(con, addr, message[5:])
      else:
        self.default(con, addr, message)
    con.close()
    return

  def kill_serv(self, con):
    # Kill server
    os._exit(1)
    return

  def helo(self, con, addr, text):
    # Reply to helo request
    reply = text.rstrip() #Remove newline
    returnString = "HELO %s\nIP:%s\nPort:%s\nStudentID:11347076" % (reply, addr[0], addr[1])
    con.sendall(returnString)
    return

  def default(self, con, addr, text):
    # Default handler for everything else
    return

def main():
  try:
    if len(sys.argv) > 1 and sys.argv[1].isdigit():
      port = int(sys.argv[1])
      server = TCPServer(port)
    else:
      server = TCPServer()
  except socket.error, msg:
    print "Unable to create socket connection: " + str(msg)
    con = None

if __name__ == "__main__": main()
