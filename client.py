# Lab2Client.py
# Lab 2 CS4032
# Cathal Geoghegan #11347076

import socket
import sys

class TCPClient:
  PORT = 8000
  HOST = '0.0.0.0'
  REQUEST = '%s'
  LENGTH = 4096

  def __init__(self, port_use=None):
    if not port_use:
      self.port_use = self.PORT
    else:
      self.port_use = port_use
    self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)


  def capString(self, string):
    '''Capitilise a string by sending a request to a server'''
    returnData = ""
    # Do nothing if the string is empty or socket doesn't exist
    if (len(string) > 0):
      # Create socket if it doesn't exist
      if  (not self.sock):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

      self.sock.connect((self.HOST, self.port_use))
      self.sock.sendall(self.REQUEST % string)

      #Loop until all data received
      while True:
        data = self.sock.recv(self.LENGTH)
        if (len(data) == 0):
          break
        returnData += data

    # Close and dereference the socket
    self.sock.close()
    self.sock = None
    return returnData


def main():
  try:
    if len(sys.argv) > 1 and sys.argv[1].isdigit():
      port = int(sys.argv[1])
      con = TCPClient(port)
    else:
      con = TCPClient()
  except socket.error, msg:
    print "Unable to create socket connection: " + str(msg)
    con = None
  while con:
    userInput = raw_input("Enter a message to send or type exit:")
    if userInput.lower() == "exit":
      con = None
    else:
      data = con.capString(userInput)
      print data

if __name__ == "__main__": main()
